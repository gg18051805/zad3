---
author: Jakub Bielański
title: Pan Tadeusz - Najważniejsza książka w polskiej literaturze
subtitle: Prezentacja
date: 
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---


## Wstęp

**_Pan Tadeusz_** jest książką, do której przeczytania zmuszony jest każdy uczeń chcący zdać maturę, nie kojarzy się więc ona zbyt dobrze większości uczniów.

Dopiero później w naszym życiu zaczynamy doceniać jej \textcolor{red}{artystyczną wartość, ponadczasowy przekaz i piękne obrazy.}, 

## Co wyróżnia książkę

Rzeczy które na tle innych polskich dzieł które wyróżniają Pana Tadeusza


* Ponadczasowy artystyczny wstęp
* Ukazanie piękna ojczystej ziemi autora
* Głębokie i dokładne opisy przyrody
* Ukazanie archetypów bohaterów
* Skomentowanie aktualnych dla Polskiarzeń



## Książka inspiracją dla malarzy

Poniżej zamieszczę kilka obrazów przedstawiających rodzimą dla Mickiewicza Litwę (Dzisiejszą Białoruś) oraz przedstawione w utworze krajobrazy

![Kostrzewski-Grzybobranie](pics/kostrzewskigrzybobranie.jpg){ height=33% width=33%} ![Kostrzewski - Polowanie](pics/kostrzewskipolowanie.jpg){ height=33% width=33%} ![Moniuszko - Koncert](pics/moniuszkokoncert.jpg){ height=33% width=33%}



## Znane cytaty z książki

„Litwo! Ojczyzno moja! ty jesteś jak zdrowie; 
Ile cię trzeba cenić, ten tylko się dowie, 
Kto cię stracił. Dziś piękność twą w całej ozdobie 
Widzę i opisuję, bo tęsknię po tobie."

#### - Adam Mickiewicz, _"Pan Tadeusz"_

_Polak, chociaż stąd między narodami słynny,
Że bardziej niźli życie kocha kraj rodzinny,
Gotów zawżdy rzucić go, puścić się w kraj świata,
W nędzy i poniewierce przeżyć długie lata,
Walcząc z ludźmi i z losem, póki mu śród burzy
Przyświeca ta nadzieja, że Ojczyźnie służy._

#### - Adam Mickiewicz, _"Pan Tadeusz"_

Zły przykład dla Ojczyzny, zachętę do zdrady 
Trzeba było okupić dobrymi przykłady, 
Krwią, poświęceniem się...

#### - Adam Mickiewicz, _"Pan Tadeusz"_

## Główni bohaterowi i ich frakcja w historii

| Soplicowie     | Horeszkowie   | Postacie niezwiązane |
| ------------- |:-------------:| -----:|
| Sędzie Soplica      | Hrabia | Wojski |
| Jacek Soplica    | Zosia Horeszko      |   Rejent |
| Tadeusz Soplica | Gerwazy      |   Asesor |

## Zakończenie

Pan Tadeusz jest darem od wspaniałego twórcy dla narodu polskiego i każdy powinien przynajmniej spróbować docenić ją za to czym jest.



